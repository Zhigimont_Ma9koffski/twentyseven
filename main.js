// function parent(a) {
//     console.log((this));
//     return function(b) {
//         console.log((this));
//         return a + b;
//     }
// }
// // const fun1 = parent(7);
// // console.log(fun1(3));
// const b = 7;
// const fun1 = parent(7);
// console.log(fun1(1));
// -----------------------------------
//1.Написать с использованием замыканий функцию,
//которая будет хранить и складывать принимаемые
//аргументы в сумму и возвращать ее.

// function parent(...arg) {
//     return function(sum) {
//         arg.forEach((count) => {
//             sum += count;
//         });
//         console.log(sum);
//     }
// }
// const sum = parent(1, 2, 3);
// console.log(sum(0));
// -----------------------------------
// function parent(...arg) {
//     let result = 0;
//     const sum = function() {
//         arg.forEach((count) => {
//             result += count;
//         });
//         return result;
//     }
//     return sum();
// }

// const sum = parent(1, 2, 3);
// console.log(sum);
// -----------------------------------

// 2. Создать кэш данных в объекте и возвращать данные из
// кэша, если значение имеется или записывать в него
// новые значения, если не имеется.

// const obj = {
//     banner: "Hello",
//     link: "google.com"
// };

// function createCache(obj) {
//     if (obj.cache) {
//         return obj.cache;
//     }
//     obj.cache = Object.keys(obj).join();
// }
// console.log(createCache(obj));
// console.log(createCache(obj));
// -----------------------------------
// 3.  Дополнить объект методом, который возвращает
// количество всех его ключей

// const obj = {
//     banner: "Hello",
//     link: "google.com"
// };

// obj.method = function() {
//     // return Object.keys(this).length;
//     let result = 0;
//     Object.keys(this).forEach((key) => {
//         if (typeof this[key] !== "function") {
//             result++;
//         }
//     });
//     return result;
// }
// console.log(obj.method());
// -----------------------------------

// const obj = {
//     banner: "Hoy",
//     link: "google.com"
// };
// console.log("test" + obj);
// obj.toString = function() {
//     return this.banner + this.link;
// };
// console.log("test" + obj);

// метод сложенія строк
// -----------------------------------
// const obj = {
//     banner: "Hoy",
//     link: "google.com"
// };
// console.log(7 + obj);
// obj.valueOf = function() {
//     return Object.keys(this).length
// };
// console.log(7 + obj);

// метод сложенія чісел

// -----------------------------------
// Создать объект машины car, который имеет свойство "передача" и
// метод, который повышает/понижает передачу на 1. Также у объекта
// должен быть метод, который в зависимости от передачи
// возвращает сообщение о скорости: (-1) - 'back', 0 - 'neutral', 1 - 'slow',
// 2 - 'medium', 3 - 'fast', 4 - 'very fast'. При попытке сложить объект car с
// числом - число должно складываться в свойство speed и speed
// должна возвращаться. Если скорость больше 5 - должно быть
// выведено сообщение "Warning! Your speed is too high!"

// const car = {
//     gear: 0,
//     ChangeGear: function(isUp) {
//         if (isUp) {
//             this.gear++;
//         } else {
//             this.gear--;
//         }
//     },
//     getGearInfo: function() {
//         let result;
//         switch (this.gear) {
//             case -1:
//                 result = "back";
//                 break;

//             case 0:
//                 result = "neutral";
//                 break;

//             case 1:
//                 result = "slow";
//                 break;

//             case 2:
//                 result = "medium";
//                 break;
//             case 3:
//                 result = "fast";
//                 break;

//             case 4:
//                 result = "very fast";
//                 break;
//         }
//         return result;
//     },
//     speed: 0,
//     valueOf: function() {
//         return this.speed;
//     }
// }

// car.speed = 6 + car.valueOf;
// console.log(car.speed);

// if (car.speed >= 5) {
//     alert("Warning! Your speed is too high!");
// }
// -----------------------------------

// деструктурізація

// const arr = ["test1", "test2"];
// const [v1, v2] = arr;
// console.log(v1);
// console.log(v2);
// console.log(v3);

// -----------------------------------
// const obj = {
//     namel: "yuo",
//     email: "qwerty@mail.com",
//     age: 101
// };
// const { name, email, age } = obj;
// console.log(namel);
// console.log(email);
// console.log(age);
// -----------------------------------
// const obj = {
//     namel: "yuo",
//     email: "qwerty@mail.com",
//     age: 101,
//     adress: {
//         street: "fgh",
//         houseNumber: 23
//     }
// };
// const { adress: { street } } = obj;
// // console.log(adress);
// console.log(street);
// -----------------------------------
// const obj = {
//     namel: "yuo",
//     email: "qwerty@mail.com",
//     age: 101,
//     adress: {
//         street: "fgh",
//         houseNumber: 23
//     }
// };
// const { adress: { street, apportaments = 15, houseNumber = 8 } } = obj;
// console.log(apportaments);
// console.log(houseNumber);
// console.log(obj);

// -----------------------------------
// const obj = {
//     namel: "yuo",
//     email: "qwerty@mail.com",
//     age: 101,
//     adress: {
//         street: (length = 1.2),
//         houseNumber: 23
//     }
// };
// const { adress: { street } } = obj;
// const { length } = street;
// console.log(test);
// console.log(obj);
// -----------------------------------
// function test(...arg) {
//     console.log(arg);
// }
// Math.max(4, 8, 58, "asfast");
// -----------------------------------

// const user1 = {
//     namel: "yuo",
//     lastName: "Johan",
//     age: 101
// };
// const user2 = {

//     namel: "yo",
//     lastName: undefined,
//     ...user1

// };
// const user3 = {
//     namel: "yuo",
//     lastName: undefined,
//     age: 14
// };
// console.log(Object.assign(user3,
//     user1, user2));
// console.log(user2);
// console.log(user3);